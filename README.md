# 12.5 pipeline creation

This is an exercise for DevOps training from [SkillFactory.ru - DEVOPS](https://lms.skillfactory.ru/)

## How To 

### to make git repo

```shell
    mkdir sf_module_12 && cd sf_module_12
    git init
    git status
    git remote add origin https://gitlab.com/dkspace/sf_12_cicd.git
    git remote -v
    git pull origin master
    git add .
    git status 
    git commit -m "repo initiated"
    git push origin master
    git commit -m "repo sinchronized"
```


### To Install gitlab-runner on CentOS

[Install GitLab runner](https://docs.gitlab.com/runner/install/linux-manually.html)

```shell
> curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
> sudo yum install gitlab-runner -y
```
[Register GitLab runner](https://docs.gitlab.com/runner/register/)
```shell
sudo gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=10931 revision=7a6612da version=13.12.0
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
xxxxxxxxxxxxxx
Enter a description for the runner:
[dkhost]: 
Enter tags for the runner (comma-separated):

Registering runner... succeeded                     runner=Exp9VYY3               
Enter an executor: parallels, shell, virtualbox, docker-ssh+machine, custom, docker-ssh, ssh, docker+machine, kubernetes, docker:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
(base) [dmik@dkhost sf_module_12]$ 

```


### to make docker

```shell 
>docker run -d --name gitlab-runner --restart always \
>      -v /srv/gitlab-runner/config:/etc/gitlab-runner \
>      -v /var/run/docker.sock:/var/run/docker.sock \
>      gitlab/gitlab-runner:latest

# to regiser runner inside docker using tags:- docker

>docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=7 revision=7a6612da version=13.12.0
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
-xxxxxxxxxxxxxxxxx
Enter a description for the runner:
[e61c3dfce418]: 
Enter tags for the runner (comma-separated):
docker
Registering runner... succeeded                     runner=-9bcCWG9
Enter an executor: docker-ssh, shell, ssh, virtualbox, docker+machine, docker-ssh+machine, custom, docker, parallels, kubernetes:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
```

